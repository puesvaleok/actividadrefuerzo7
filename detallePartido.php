<?php
$conexion = new mysqli("localhost", "root", "", "liga");
if ($conexion->connect_errno) {
    echo "Fallo al conectar a MySQL: (" . $conexion->connect_errno . ") " . $conexion->connect_error;
}else{
  $id=$_GET["idEquipo"];
  $resultado = $conexion->query("SELECT * FROM partido WHERE local=".$id);
    $resultadovisitante = $conexion->query("SELECT * FROM partido WHERE visitante=".$id);
}
 ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  </head>
  <body>
          <nav>
            <ul>
              <li style="color:blue;margin-left: 70px; float:left;"><a href="equipos.php">Equipos Listado equipos</a></li>
              <li style="color:blue;margin-left: 300px; float:left;"><a href="jugadores.php">Equipos Listado jugadores</a></li>
              <li style="color:blue;margin-left: 500px;"><a href="partidos.php">Equipos Listado partidos</a></li>
            </ul>
          </nav>
      <table>
        <tr>
            <td style="color:white;text-align:center;background-color:#2E9AFE">ID partido</td>
            <td style="color:white;text-align:center;background-color:#2E9AFE">Local</td>
            <td style="color:white;text-align:center;background-color:#2E9AFE">Visitante</td>
            <td style="color:white;text-align:center;background-color:#2E9AFE">Resultado</td>
            <td style="color:white;text-align:center;background-color:#2E9AFE">Fecha</td>
            <td style="color:white;text-align:center;background-color:#2E9AFE">Arbitro</td>
        </tr>
        <?php
          foreach ($resultado as $partido) {
            echo "<tr>";
            echo "<td style=text-align:center;>".$partido['id_partido']."</td>";
            echo "<td style=text-align:center;>".$partido['local']."</td>";
            echo "<td style=text-align:center;>".$partido['visitante']."</td>";
            echo "<td style=text-align:center;>".$partido['resultado']."</td>";
              echo "<td style=text-align:center;>".$partido['fecha']."</td>";
                echo "<td style=text-align:center;>".$partido['arbitro']."</td>";
            echo "</tr>";
          }
        ?>
        <?php
          foreach ($resultadovisitante as $partido) {
            echo "<tr>";
            echo "<td style=text-align:center;>".$partido['id_partido']."</td>";
            echo "<td style=text-align:center;>".$partido['local']."</td>";
            echo "<td style=text-align:center;>".$partido['visitante']."</td>";
            echo "<td style=text-align:center;>".$partido['resultado']."</td>";
              echo "<td style=text-align:center;>".$partido['fecha']."</td>";
                echo "<td style=text-align:center;>".$partido['arbitro']."</td>";
            echo "</tr>";
          }
        ?>
      </table>
    </div>
  </body>
</html>
